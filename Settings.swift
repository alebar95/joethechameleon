//
//  Settings.swift
//  Speekeezy
//
//  Created by Alessandro Barruffo on 05/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import Foundation

class Settings {

    var isAudioOn : Bool = true
    
    static var shared = Settings()
    
    private init() {
        
    }
    
}
