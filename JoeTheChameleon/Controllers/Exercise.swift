//
//  Exercise.swift
//  Speekeezy
//
//  Created by Andrea Cascella on 08/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation



class Exercise : DraggableViewDelegate{
    
    var phrase: [Part] = []
    var card = CardView()
    var cell = CellView()

    
    var cards : [CardView] = []
    var cells : [CellView] = []
    
    var cardsCellsDict : [CardView: CellView] = [:]
    var cardsCentersDict : [CardView : CGPoint] = [:]
    
    var compArray : [PartType] = []
    var referenceArray: [PartType] = []
    
    var name: String
    var level = 0 
    
    init(name: String){
        self.name = name
    }
    

    func exerciseLogic(errorPlayer: AVAudioPlayer){
        
        if (card.frame.intersects(cardsCellsDict[card]!.frame)) && compareThings(card: card, cell: cardsCellsDict[card]!) == true{
            cardMoveToCell(card: card, cell: cardsCellsDict[card]!)
            if compArray.count < phrase.count {
                compArray.append(card.cardType!)
                compareOrder(card: card, errorPlayer: errorPlayer)
                
            }
        } else {
            cardMoveToOrigin(card: card, origin: card.resetPosition!)
            errorPlayer.play()
        }
        
    }
    
    func compareOrder(card: CardView,errorPlayer: AVAudioPlayer){
        if compArray.count >= 0 {
            for i in 0...compArray.count-1 {
                if compArray[i] == referenceArray[i] {
                    
                    card.isUserInteractionEnabled = !(card.cardType == compArray[i])
                    
          
                } else {
                    
                    compArray.remove(at: i)
                    errorPlayer.play()
                    cardMoveToOrigin(card: card, origin: card.resetPosition!)
                  
                }
            }
        }
    }
    
    func populateReference(){
        for i in 0...phrase.count-1 {
            referenceArray.append(phrase[i].partType)
        }
        print(referenceArray)
    }
    
    func compareToReference() -> Bool{
        return compArray == referenceArray
    }
    
    func compareThings(card: CardView, cell: CellView) -> Bool{
        if card.frame.intersects(cell.frame) && card.center != cell.center{
            if card.cardType == cell.cellType{
                return true
            } else {
                return false
            }
            
        }
        return false
    }
    
    func cardMoveToCell(card: CardView, cell: CellView){
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
            card.center = cell.center
            
        }, completion: nil)
        
    }
    
    func cardMoveToOrigin(card: CardView, origin: CGPoint){
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
            card.center = origin
        }, completion: nil)
        
    }
    
    func increaseLevel() {
        level += 1
    }
    
    func checkLevel(levelNumber: Int ) -> Bool {
        return level == levelNumber
    }
    
}
