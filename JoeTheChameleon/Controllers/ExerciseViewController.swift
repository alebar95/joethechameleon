//
//  TryExerciseViewController.swift
//  Speekeezy
//
//  Created by Andrea Cascella on 06/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit
import AVFoundation



class ExerciseViewController: UIViewController, DraggableViewDelegate {
       
    @IBOutlet weak var exerciseView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var audioButton: UIButton!
    @IBOutlet weak var exerciseTitleLabel: UILabel!
    
    let imageView = UIImageView(image: #imageLiteral(resourceName: "Well done!"))
    let transition = RevealAnimator()
    var repeatButton = UIButton()
    var nextButton = UIButton()
    var errorAudioPlayer = AVAudioPlayer()
    var successAudioPlayer = AVAudioPlayer()
    var cards : [CardView] = []
    var cells : [CellView] = []
    
    var cardsCellsDict : [CardView: CellView] = [:]
    var cardsCentersDict : [CardView : CGPoint] = [:]
    
    let phrase = [Part(partType: .subject, partName: "Joe"), Part(partType: .verb, partName: "Goes to"), Part(partType: .object, partName: "The kindergarten")]
    
    var exercise1 = Exercise(name: "Exercise1")
    
    func setConstraints() {
        exerciseTitleLabel.adjustsFontSizeToFitWidth = true
        exerciseView.translatesAutoresizingMaskIntoConstraints = false
        exerciseView.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 0.730469).isActive = true
        exerciseView.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 0.787109).isActive = true
        exerciseView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        exerciseView.topAnchor.constraint(equalTo: exerciseTitleLabel.bottomAnchor, constant: 8).isActive = true
        exerciseView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        repeatButton.translatesAutoresizingMaskIntoConstraints = false
        repeatButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -85).isActive = true
        repeatButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 300).isActive = true
        repeatButton.widthAnchor.constraint(equalTo: backButton.widthAnchor).isActive = true
        repeatButton.heightAnchor.constraint(equalTo: backButton.heightAnchor).isActive = true
        
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 85).isActive = true
        nextButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 300).isActive = true
        nextButton.widthAnchor.constraint(equalTo: backButton.widthAnchor).isActive = true
        nextButton.heightAnchor.constraint(equalTo: backButton.heightAnchor).isActive = true
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        audioSetting()
        view.addSubview(repeatButton)
        view.addSubview(nextButton)
        setConstraints()
        repeatButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
        repeatButton.tag = 1
        repeatButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        repeatButton.alpha = 0.0
     
        
        if exercise1.level != 2{
            nextButton.setImage(#imageLiteral(resourceName: "icona forward.png"), for: .normal)
        } else {
            nextButton.setImage(#imageLiteral(resourceName: "home"), for: .normal)
        }
        
        nextButton.tag = 2
        nextButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        nextButton.alpha = 0.0
        
        
        imageView.center.x = view.center.x
        imageView.center.y = view.center.y+1000
        
    
        exerciseView.backgroundColor = .white
        
        summonThings(phrase: phrase, exercise: exercise1)
        
        exercise1.phrase = phrase
        
        
        for card in cards {
            exercise1.card = card
        }
        
        for cell in cells{
            exercise1.cell = cell
        }
        
        exercise1.cardsCellsDict = cardsCellsDict
        
        exercise1.populateReference()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func summonThings(phrase: [Part], exercise: Exercise){
        
        let numberOfElements = phrase.count
       
        let exerciseViewSize = exerciseView.systemLayoutSizeFitting(CGSize(width: view.bounds.width*0.787109, height: view.bounds.height*0.730469))
        
        let width = exerciseViewSize.width / CGFloat(numberOfElements)
        let height = exerciseViewSize.height / 2.0

        var centerX =  width / 2.0
        let centerY = height / 2.0
        
        for i in 0...numberOfElements-1 {
            
            print(numberOfElements)
            
            let cellCenter = CGPoint(x: centerX, y: centerY*1.1)
            let cardCenter = CGPoint(x: centerX, y: (centerY*1.1) + height)
            //            let size = CGSize(width: width*0.5, height: height*0.7)
            let size = CGSize(width: width*0.8, height: height*0.9)

            
            let cell = makeCell(part: phrase[i], center: cellCenter, size: size)
            let card = makeCard(part: phrase[i], center: cardCenter, exercise: exercise)
            
            print("created \(cell) at \(cellCenter)")
            print("created \(card) at \(cardCenter)")
            print("-----")
            
            card.frame.size = cell.frame.size
            
            cards.append(card)
            cells.append(cell)
            cardsCellsDict[card] = cell
            
            exerciseView.addSubview(cell)
            exerciseView.sendSubviewToBack(cell)
            exerciseView.addSubview(card)
            exerciseView.bringSubviewToFront(card)
            
            centerX += width
        }
        
        var tempOrigins : [CGPoint] = []
        
        
        for card in cards{
            tempOrigins.append(card.resetPosition!)
            
        }
        tempOrigins.shuffle()
        while tempOrigins[0] == cards[0].center{
            tempOrigins.shuffle()
        }
        
        for card in 0...cards.count-1{
            cardsCentersDict.updateValue(tempOrigins[card], forKey: cards[card])
            cards[card].center = cardsCentersDict[cards[card]]!
            cards[card].resetPosition = cards[card].center
        }
        
    }
    
    func makeCard(part: Part, center: CGPoint, exercise: Exercise) -> CardView {
        
        let myCard = CardView()
        
        
        myCard.center = center
        myCard.resetPosition = center
        
        myCard.backgroundColor = .clear
        myCard.card.backgroundColor = part.color
                
        myCard.cardImage.image = UIImage(named: "\(exercise.name)/\(part.partType)")
        myCard.cardImage.dropShadow(scale: true)
        myCard.sendSubviewToBack(myCard.cardImage)
        
        myCard.cardText(string: part.partName )
        
        myCard.cardType = part.partType
        
        myCard.delegate = self
        
        return myCard
        
    }
    
    func makeCell(part: Part, center: CGPoint, size: CGSize) -> CellView {
        
        let cell = CellView()
        
        cell.frame.size = size
        cell.center = center
        
        cell.cellType = part.partType
        
        if exercise1.level == 0 {
            cell.cellLabel.text = String(part.question)
        }
        else {
            cell.cellLabel.text = ""
        }
                
        cell.cellLabel.textColor = part.color
        
        if exercise1.level != 2 {
            cell.addDashedBorder(color: part.color)
        } else {
            cell.addDashedBorder(color: .darkGray)
        }
        return cell
        
    }
    
    func panGestureDidBegin(_ panGesture: UIPanGestureRecognizer, originalCenter: CGPoint) {
        
        
        print("began")
    }
    
    func panGestureDidEnd(_ panGesture: UIPanGestureRecognizer, originalCenter: CGPoint, translation: CGPoint, velocityInView: CGPoint) {
        print(originalCenter)
        
        exercise1.card = (panGesture.view as? CardView)!
        
        exercise1.exerciseLogic(errorPlayer: errorAudioPlayer)
        
        if exercise1.compareToReference() == true {
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5 , initialSpringVelocity: 1.0, options: [.curveEaseOut, .transitionFlipFromBottom], animations: {
                self.view.addSubview(self.imageView)
                self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
                self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor,constant: 0).isActive = true
                self.view.layoutIfNeeded()
                self.successAudioPlayer.play()
                
            }) { _ in
                self.nextButton.alpha = 1.0
                self.repeatButton.alpha = 1.0
            }
            
            
            
        }
        
    }
 
        @IBAction func onClickButton(_ sender: UIButton) {
            
            let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
            self.navigationController?.show(storySceneVC, sender: nil)
            
        }
    
    @IBAction func onClickMute_UnMute(_ sender: UIButton) {
        if Settings.shared.isAudioOn {
                  audioButton.setImage(#imageLiteral(resourceName: "icona no volume"), for: .normal)
                  errorAudioPlayer.volume = 0
                  successAudioPlayer.volume = 0
              } else {
                  audioButton.setImage(#imageLiteral(resourceName: "icona volume attivo"), for: .normal)
                  errorAudioPlayer.volume = 1
                  successAudioPlayer.volume = 0.5
              }
        Settings.shared.isAudioOn.toggle()
    }
    
    func audioSetting() {
        do {
            successAudioPlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "success_3", ofType: "wav")!)!)
            errorAudioPlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "errorBuzz", ofType: "wav")!)!)
            successAudioPlayer.prepareToPlay()
            errorAudioPlayer.prepareToPlay()
            
        } catch {
            print(error)
        }
        if Settings.shared.isAudioOn {
            audioButton.setImage(#imageLiteral(resourceName: "icona volume attivo"), for: .normal)
            errorAudioPlayer.volume = 1
            successAudioPlayer.volume = 0.5
        } else {
            audioButton.setImage(#imageLiteral(resourceName: "icona no volume"), for: .normal)
            errorAudioPlayer.volume = 0
            successAudioPlayer.volume = 0
        }
    }
    
    @objc func buttonAction(sender: UIButton) {
        
        if sender.tag == 1 {
            let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
            self.navigationController?.show(storySceneVC, sender: nil)
        } else if sender.tag == 2 {
            print("next tapped")
            let exerciseVC = UIStoryboard(name: "TryStoryboard", bundle: nil).instantiateViewController(withIdentifier: "TryExerciseVC") as! ExerciseViewController
            let exercise = self.exercise1
            if exercise.checkLevel(levelNumber: 0) {
                exerciseVC.exercise1.level = 1
                self.navigationController?.show(exerciseVC, sender: nil)
            } else if exercise.checkLevel(levelNumber: 1) {
                exerciseVC.exercise1.level = 2
                self.navigationController?.show(exerciseVC, sender: nil)
            } else {
                let homeSceneVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeSceneVC") as! HomeViewController
                self.navigationController?.show(homeSceneVC, sender: nil)
                
            }
            
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
           transition.operation = operation
           return transition
       }
    
    }
 

