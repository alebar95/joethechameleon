//
//  StoryViewController.swift
//  JoeTheChameleon
//
//  Created by Alessandro Barruffo on 23/02/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit
import AVFoundation
import Lottie

class StoryViewController: UIViewController {
    
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var audioButton: UIButton!
    @IBOutlet weak var sunImageView: UIImageView!
 
    let maskImageView = UIImageView()
    let chameleonLayer = CALayer()
    
    var stepPlayer = AVAudioPlayer()
    var ambiencePlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
        audioSetting()
      
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.leadingAnchor.constraint(equalTo: backGroundView.leadingAnchor, constant: backGroundView.systemLayoutSizeFitting(CGSize(width: view.frame.width*0.790741, height: view.frame.height*0.708642)).width*0.289227/5).isActive = true
//        self.imageView.center = CGPoint(x: 310+self.imageView.frame.width/2, y: 473+self.imageView.frame.height/2)
        
        backButton.isHidden = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        imageView.image = Character.shared.image
        maskImageView.image = UIImage(named: "chameleon_Mask")
        maskImageView.frame = imageView!.bounds
        imageView.mask = maskImageView
        
        chameleonLayer.contents = UIImage(named: "chameleon_Layer")?.cgImage
        chameleonLayer.frame = imageView.bounds
        imageView.layer.addSublayer(chameleonLayer)
        backgroundMusicPlayer?.stop()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        maskImageView.image = UIImage(named: "chameleon_Mask")
        maskImageView.frame = imageView!.bounds
        imageView.mask = maskImageView
        
        chameleonLayer.contents = UIImage(named: "chameleon_Layer")?.cgImage
        chameleonLayer.frame = imageView.bounds
        imageView.layer.addSublayer(chameleonLayer)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        ambiencePlayer.stop()
        stepPlayer.stop()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        audioSetting()
        chameleonGoing()
        UIView.animate(withDuration: 5.0, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.sunImageView.transform = CGAffineTransform(rotationAngle: 1)
        }, completion: nil)
    }
    
    
    
    @IBAction func onClickNext(_ sender: Any) {
        let exerciseVC = UIStoryboard(name: "TryStoryboard", bundle: nil).instantiateViewController(withIdentifier: "TryExerciseVC") as! ExerciseViewController
        self.navigationController?.pushViewController(exerciseVC, animated: true)
    }
    
    @IBAction func onClickHome(_ sender: UIButton) {
        let homeSceneVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeSceneVC") as! HomeViewController
        self.navigationController?.show(homeSceneVC, sender: nil)
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
    }

    func chameleonGoing() {
        do {
            stepPlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "Chapter1_steps", ofType: "mp3")!)!)
//            ambiencePlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "Chapter1_ambience", ofType: "mp3")!)!)
//            ambiencePlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "Chapter1_ambience", ofType: "mp3", inDirectory: "Sounds")!)!)
            
            
            ambiencePlayer.volume = 0.05
            stepPlayer.prepareToPlay()
            ambiencePlayer.prepareToPlay()
        } catch {
            print(error)
        }
        if Settings.shared.isAudioOn {
             stepPlayer.play()
            ambiencePlayer.play()
        }
        UIView.animateKeyframes(withDuration: 5.0, delay: 0.0,animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.1,animations: {
                self.imageView.center.x += 25.0
                self.imageView.center.y -= 10.0
            })
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.25) {
                self.imageView.transform = CGAffineTransform(rotationAngle: -.pi / 32)
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.30) {
                self.imageView.center.x += 80.0
                self.imageView.center.y -= 40.0
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.1) {
                self.imageView.center.x += 25.0
                self.imageView.center.y -= 20.0
            }
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.15) {
                self.imageView.transform = CGAffineTransform(rotationAngle: -.pi / 16)
                self.imageView.center.y -= 20.0
            }
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.1) {
                self.imageView.center.y -= 20.0
                self.imageView.transform = CGAffineTransform(rotationAngle: .pi / 16)
                
            }
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.1,animations: {
                self.imageView.center.x += 25.0
                self.imageView.center.y -= 10.0
            })
            
            
        },completion: nil)
    }
    
 
    @IBAction func onClickMute_UnMute(_ sender: UIButton) {
        if Settings.shared.isAudioOn {
                  print("disabling audio")
                         sender.setImage(#imageLiteral(resourceName: "icona no volume"), for: .normal)
                         stepPlayer.volume = 0
                         ambiencePlayer.volume = 0
                     } else {
                  print("enabling audio")
                         sender.setImage(#imageLiteral(resourceName: "icona volume attivo"), for: .normal)
                         stepPlayer.volume = 1
                  ambiencePlayer.volume = 0.05
                     }
              Settings.shared.isAudioOn.toggle()
    }
    
    func audioSetting() {
          if Settings.shared.isAudioOn {
              audioButton.setImage(#imageLiteral(resourceName: "icona volume attivo"), for: .normal)
              stepPlayer.volume = 1
            ambiencePlayer.volume = 1
          } else {
              audioButton.setImage(#imageLiteral(resourceName: "icona no volume"), for: .normal)
              stepPlayer.volume = 0
            ambiencePlayer.volume = 0
          }
      }
 
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


