//
//  Pencil.swift
//  JoeTheChameleon
//
//  Created by Alessandro Barruffo on 23/02/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//
import Foundation
import UIKit

enum Pencil {
    case black
    case brown
    case pink
    case purple
    case violet
    case darkBlue
    case lightBlue
    case lightGreen
    case darkGreen
    case yellow
    case orange
    case lightRed
    case darkRed
    
    var color: UIColor {
        switch self {
        case .black:
            return .white
        case .brown:
            return .brown
        case .pink:
            return UIColor(red: 252/255.0, green: 172/255.0,blue: 1,alpha: 1.0)
        case .purple:
            return UIColor(red: 224/255.0, green: 15/255.0,blue: 90/255.0,alpha: 1.0)
        case .violet:
            return UIColor(red: 126/255.0, green: 77/255.0, blue: 198/255.0, alpha: 1.0)
        case .darkBlue:
            return UIColor(red: 48/255.0, green: 67/255.0,blue: 1,alpha: 1.0)
        case .lightBlue:
            return UIColor(red: 60/255.0, green: 159/255.0, blue: 253/255.0, alpha: 1.0)
        case .lightGreen:
            return UIColor(red: 112/255.0, green: 197/255.0, blue: 23/255.0, alpha: 1.0)
        case .darkGreen:
            return UIColor(red: 63/255.0, green: 165/255.0, blue: 70/255.0, alpha: 1.0)
        case .yellow:
            return UIColor(red: 1, green: 1, blue: 0, alpha: 1.0)
        case .orange:
            return .orange
        case .lightRed:
            return UIColor(red: 238/255.0, green: 72/255.0,blue: 16/255.0,alpha: 1.0)
        case .darkRed:
            return .red
        }
    }
    
    init?(tag: Int) {
        switch tag {
        case 1:
            self = .black
        case 2:
            self = .brown
        case 3:
            self = .pink
        case 4:
            self = .purple
        case 5:
            self = .violet
        case 6:
            self = .darkBlue
        case 7:
            self = .lightBlue
        case 8:
            self = .lightGreen
        case 9:
            self = .darkGreen
        case 10:
            self = .yellow
        case 11:
            self = .orange
        case 12:
            self = .lightRed
        case 13:
            self = .darkRed
        default:
            return nil
        }
    }
    
    
}

