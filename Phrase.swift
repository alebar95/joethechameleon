//
//  Phrase.swift
//  Speekeezy
//
//  Created by Andrea Cascella on 06/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit
import Foundation


enum PartType{
    case subject, verb, object
    
    init?(tag: Int){
        switch tag{
        case 1:
            self = .subject
        case 2:
            self = .verb
        case 3:
            self = .object
        default:
            return nil
        
        }
        
        
    }
    
    
    
}




class Part {
    
    var color: UIColor
    var partType: PartType
    var partName: String
    var question: String
    
    init(partType: PartType, partName: String) {
        
        self.partName = partName
        
        self.partType = partType
        switch partType {
        case .subject:
            self.color = UIColor(red:0.55, green:0.87, blue:0.42, alpha:1.0)
            self.question = "Who?"
        case .verb:
            self.color = UIColor(red:0.95, green:0.36, blue:0.39, alpha:1.0)
            self.question = "Does what?"
        case .object:
            self.color = UIColor(red:1.00, green:0.83, blue:0.40, alpha:1.0)
            self.question = "Where?"
        }
    }
    
    
}


