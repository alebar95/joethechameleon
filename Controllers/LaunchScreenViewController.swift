//
//  LaunchScreenViewController.swift
//  Speekeezy
//
//  Created by Alessandro Barruffo on 04/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit
import Lottie

class LaunchScreenViewController: UIViewController, UINavigationControllerDelegate {
    
//    let animationView = AnimationView()
    let transition = RevealAnimator()
    let imageView = UIImageView(image: #imageLiteral(resourceName: "Launch screen immagine-1"))
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        view.addSubview(imageView)
        
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.center.x = view.center.x
        imageView.center.y = view.center.y+1000
        /*
        let animation = Animation.named("splash")
        animationView.animation = animation
        view.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        animationView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        animationView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        animationView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
 */
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: [.transitionCurlDown], animations: {
             
             self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
             self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor,constant: 0).isActive = true
             self.view.layoutIfNeeded()
        }) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let homeSceneVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeSceneVC") as! HomeViewController
                self.navigationController?.pushViewController(homeSceneVC, animated : true)
            }
           
        }
       /* animationView.play(completion: { (finished) in
             let homeSceneVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeSceneVC") as! HomeViewController
             self.navigationController?.pushViewController(homeSceneVC, animated : true)
        })
 */
    }
    
  /*  func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.operation = operation
        return transition
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
