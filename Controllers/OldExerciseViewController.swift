//
//  ExerciseViewController.swift
//  JoeTheChameleon
//
//  Created by Alessandro Barruffo on 23/02/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit
import AVFoundation
import NotificationCenter

class OldExerciseViewController: UIViewController, UIDropInteractionDelegate, UIDragInteractionDelegate, UINavigationControllerDelegate {
    
  
    @IBOutlet weak var exampleCard: CardView!
    
    @IBOutlet weak var greenBoardImageView: UIImageView!
    @IBOutlet weak var redBoardImageView: UIImageView!
    @IBOutlet weak var yellowBoardImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var yellowBorder: UIImageView!
    @IBOutlet weak var redBorder: UIImageView!
    @IBOutlet weak var greenBorder: UIImageView!
    @IBOutlet weak var yellowView: UIView!
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var greenBorderView: UIView!
    @IBOutlet weak var yellowBorderView: UIView!
    @IBOutlet weak var redBorderView: UIView!
    @IBOutlet weak var yellowCardImageView: UIImageView!
    @IBOutlet weak var redCardImageView: UIImageView!
    @IBOutlet weak var greenCardImageView: UIImageView!
    
    
    let transition = RevealAnimator()
    var repeatButton = UIButton()
    var nextButton = UIButton()
    let imageView = UIImageView(image: #imageLiteral(resourceName: "Well done!"))
    var cardImageViews  = [UIImageView]()
    var borderImageViews = [UIImageView?](repeating: nil, count: 3)
    var cardViews = [UIView]()
    var audioPlayer = AVAudioPlayer()
    var infoDebug : Bool = false
    var exerciseCount = 0
    var exerciseLevel = 0
    var dragInteraction : UIDragInteraction?
    var steps = [true,false,false]
    
    var panGestureRecognizer: UIPanGestureRecognizer?
    var originalPosition: CGPoint?
    
    
 override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     navigationController?.setNavigationBarHidden(true, animated: animated)
 }

 override func viewWillDisappear(_ animated: Bool) {
     super.viewWillDisappear(animated)
     navigationController?.setNavigationBarHidden(false, animated: animated)
 }
    

    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
        panGestureRecognizer = UIPanGestureRecognizer(target: exampleCard, action: #selector(panGestureAction(_:)))
        exampleCard.addGestureRecognizer(panGestureRecognizer!)
        exampleCard.isUserInteractionEnabled = true
        
        exampleCard.layer.masksToBounds = false
        exampleCard.cardImage.image = UIImage(named: "Kindergarten_alpha")
        exampleCard.cardText(string: "Prova")
        
        
        dragInteraction = UIDragInteraction(delegate: self)
        for i in 0 ..< 3 {
            if borderImageViews[i] != nil {
                
                if i == 0 {
                    greenBoardImageView.image = borderImageViews[i]?.image
                } else if i == 1 {
                    redBoardImageView.image = borderImageViews[i]?.image
                } else if i == 2 {
                    yellowBoardImageView.image = borderImageViews[i]?.image
                }
            }
        }
        repeatButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
        repeatButton.tag = 1
        repeatButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        view.addSubview(repeatButton)
        repeatButton.alpha = 0.0
        repeatButton.translatesAutoresizingMaskIntoConstraints = false
        repeatButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -85).isActive = true
        repeatButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 300).isActive = true
        repeatButton.widthAnchor.constraint(equalTo: backButton.widthAnchor).isActive = true
        repeatButton.heightAnchor.constraint(equalTo: backButton.heightAnchor).isActive = true
        
        nextButton.setImage(#imageLiteral(resourceName: "icona forward.png"), for: .normal)
        nextButton.tag = 2
        nextButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        view.addSubview(nextButton)
        nextButton.alpha = 0.0
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 85).isActive = true
        nextButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 300).isActive = true
        nextButton.widthAnchor.constraint(equalTo: backButton.widthAnchor).isActive = true
        nextButton.heightAnchor.constraint(equalTo: backButton.heightAnchor).isActive = true
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.center.x = view.center.x
        imageView.center.y = view.center.y+1000
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        cardImageViews = [yellowCardImageView,redCardImageView,greenCardImageView]
        cardViews = [greenBorderView,yellowBorderView,redBorderView]
        for imageView in cardImageViews{
            imageView.layer.cornerRadius = 15.0
            imageView.isUserInteractionEnabled = true
        }
        for view in cardViews {
            customEnableDragging(on: view, dragInteractionDelegate: self)
            customEnableDropping(on: view, dropInteractionDelegate: self)
            view.layer.cornerRadius = 15.0
        }
        view.addInteraction(dragInteraction!)
        customEnableDropping(on: view, dropInteractionDelegate: self)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "errorBuzz", ofType: "wav")!)!)
            audioPlayer.prepareToPlay()
        } catch {
            print(error)
        }
        let name = Notification.Name("exerciseGood")
        NotificationCenter.default.addObserver(self, selector: #selector(onGotRight), name: name, object: nil)
    }
    

    
    func customEnableDragging(on view: UIView, dragInteractionDelegate: UIDragInteractionDelegate) {
        let dragInteraction = UIDragInteraction(delegate: dragInteractionDelegate)
        view.addInteraction(dragInteraction)
    }
    
    
    func customEnableDropping(on view: UIView, dropInteractionDelegate: UIDropInteractionDelegate) {
        let dropInteraction = UIDropInteraction(delegate: dropInteractionDelegate)
        view.addInteraction(dropInteraction)
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        
        let touchedPoint = session.location(in: self.view)
        
        if let touchedImageView = self.view.hitTest(touchedPoint, with: nil) as? UIImageView {
            let touchedImage = touchedImageView.image
            let imageItemProvider = NSItemProvider(object: touchedImage!)
            let dragItem = UIDragItem(itemProvider: imageItemProvider)
            dragItem.localObject = touchedImageView
            return [dragItem]
        }
       return []
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        UITargetedDragPreview(view: item.localObject as! UIView)
    }
    
   func dragInteraction(_ interaction: UIDragInteraction, willAnimateLiftWith animator: UIDragAnimating, session: UIDragSession) {
       session.items.forEach { (dragItem) in
          if let touchedImageView = dragItem.localObject as? UIView {
            touchedImageView.removeFromSuperview()
            }
       }
   }

    func dragInteraction(_ interaction: UIDragInteraction, item: UIDragItem, willAnimateCancelWith animator: UIDragAnimating) {
        let imageView = item.localObject as! UIImageView
        guard let image = imageView.image else {
            return
        }
        guard let draggedData = image.pngData() else {
            return
        }
        if draggedData == #imageLiteral(resourceName: "Carta verde 1").pngData() {
            self.greenView.addSubview(imageView)
            imageView.centerXAnchor.constraint(equalTo: greenCardImageView.centerXAnchor).isActive = true
        } else if draggedData == #imageLiteral(resourceName: "carta rossa").pngData() {
            self.redView.addSubview(imageView)
        } else if draggedData == #imageLiteral(resourceName: "carta gialla").pngData() {
            self.yellowView.addSubview(imageView)
        }
    }
    
    
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: UIImage.self)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .move)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
    
        session.loadObjects(ofClass: UIImage.self) { imageItems in
            guard let draggedImage = imageItems.first as? UIImage else {
                return
            }
            
            guard let draggedImageData = draggedImage.pngData() else {
                return
            }
            let imageView = UIImageView(image: draggedImage)
            
            imageView.isUserInteractionEnabled = true
            imageView.translatesAutoresizingMaskIntoConstraints = false
            let detouchedPoint = session.location(in: self.view)
            let xPosition = detouchedPoint.x
            let yPosition = detouchedPoint.y
            
            let greenBorderViewMinX = self.getMinX(view: self.greenBorderView)
            let greenBorderViewMaxX = self.getMaxX(view: self.greenBorderView)
            let greenBorderViewMinY = self.getMinY(view: self.greenBorderView)
            let greenBorderViewMaxY = self.getMaxY(view: self.greenBorderView)
            let redBorderViewMinX = self.getMinX(view: self.redBorderView)
            let redBorderViewMaxX = self.getMaxX(view: self.redBorderView)
            let redBorderViewMinY = self.getMinY(view: self.redBorderView)
            let redBorderViewMaxY = self.getMaxY(view: self.redBorderView)
            let yellowBorderViewMinX = self.getMinX(view: self.yellowBorderView)
            let yellowBorderViewMaxX = self.getMaxX(view: self.yellowBorderView)
            let yellowBorderViewMinY = self.getMinY(view: self.yellowBorderView)
            let yellowBorderViewMaxY = self.getMaxY(view: self.yellowBorderView)
            
            DispatchQueue.main.async {
                self.view.addSubview(imageView)
                print(imageView.frame)
                if xPosition >= greenBorderViewMinX && xPosition <= greenBorderViewMaxX && yPosition >= greenBorderViewMinY && yPosition <= greenBorderViewMaxY && self.steps[0] {
                    print("nel verde")
                    self.greenBorder.addSubview(imageView)
                    self.adjustDroppedImageView(imageView: imageView, view: self.greenBorder)
                    self.steps[0].toggle()
                    self.steps[1].toggle()
                } else if xPosition >= redBorderViewMinX && xPosition <= redBorderViewMaxX && yPosition >= redBorderViewMinY && yPosition <= redBorderViewMaxY  && self.steps[1] {
                    print("nel rosso")
                    self.redBorder.addSubview(imageView)
                    self.adjustDroppedImageView(imageView: imageView, view: self.redBorder)
                    self.steps[1].toggle()
                    self.steps[2].toggle()
                } else if xPosition >= yellowBorderViewMinX && xPosition <= yellowBorderViewMaxX && yPosition >= yellowBorderViewMinY && yPosition <= yellowBorderViewMaxY && self.steps[2] {
                    print("nel giallo")
                    self.yellowBorder.addSubview(imageView)
                    self.adjustDroppedImageView(imageView: imageView, view: self.yellowBorder)
                    self.steps[0].toggle()
                    self.steps[1].toggle()
                } else {
                    print("fuori")
                 
                    let imageView = session.items.first!.localObject as! UIImageView
                    guard let draggedData = imageView.image!.pngData() else {
                        return
                    }
                    if draggedData == #imageLiteral(resourceName: "Carta verde").pngData() {
                        self.greenView.addSubview(imageView)
                    } else if draggedData == #imageLiteral(resourceName: "Carta rossa").pngData() {
                        self.redView.addSubview(imageView)
                    } else if draggedData == #imageLiteral(resourceName: "carta gialla").pngData() {
                        self.yellowView.addSubview(imageView)
                    }
                }
                
                if draggedImageData == #imageLiteral(resourceName: "Carta verde").pngData()! {
                    if imageView.superview == self.greenBorder {
                        self.exerciseCount += 1
                        if self.checkExercise() {
                            self.sendNotification()
                        }
                        
                    }  else {
                        self.audioPlayer.play()
                        imageView.removeFromSuperview()
                        self.greenView.addSubview(session.items.first!.localObject as! UIView)
                    }
                } else if draggedImageData == #imageLiteral(resourceName: "Carta rossa").pngData()! {
                    if imageView.superview == self.redBorder {
                        self.exerciseCount += 1
                        if self.checkExercise() {
                            self.sendNotification()
                        } else if imageView.superview == self.redView {
                            
                        }
                    } else {
                        self.audioPlayer.play()
                        imageView.removeFromSuperview()
                        self.redView.addSubview(session.items.first!.localObject as! UIView)
                    }
                    
                } else if draggedImageData == #imageLiteral(resourceName: "carta gialla").pngData()! {
                    if imageView.superview == self.yellowBorder {
                        self.exerciseCount += 1
                        if self.checkExercise() {
                            self.sendNotification()
                        }
                    } else {
                        self.audioPlayer.play()
                        imageView.removeFromSuperview()
                        self.yellowView.addSubview(session.items.first!.localObject as! UIView)
                    }
                }
            }
        }
    }
    
    func getMinX(view: UIView) -> CGFloat {
        return view.frame.minX
    }
    func getMaxX(view: UIView) -> CGFloat {
        return view.frame.maxX
    }
    func getMinY(view: UIView) -> CGFloat {
        return view.frame.minY
    }
    func getMaxY(view: UIView) -> CGFloat {
        return view.frame.maxY
    }
    
    func checkExercise() -> Bool {
        return self.exerciseCount == 3
    }
    func sendNotification() {
        let name = Notification.Name("exerciseGood")
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    func adjustDroppedImageView(imageView: UIImageView, view: UIView){
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    @objc func onGotRight() {
        print("Great, you got the exercise right!!")
         do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "success_3", ofType: "wav")!)!)
            self.audioPlayer.volume = 0.5
            self.audioPlayer.prepareToPlay()
         } catch {
             print(error)
         }
        view.removeInteraction(self.dragInteraction!)
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5 , initialSpringVelocity: 1.0, options: [.curveEaseOut, .transitionFlipFromBottom], animations: {
            self.view.addSubview(self.imageView)
            self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
            self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor,constant: 0).isActive = true
            self.view.layoutIfNeeded()
            self.audioPlayer.play()
        }) { _ in
             self.repeatButton.alpha = 1.0
             self.nextButton.alpha = 1.0
        }
    }
    
    @objc func buttonAction(sender: UIButton) {
//          let exerciseVC = UIStoryboard(name: "Exercise", bundle: nil).instantiateViewController(withIdentifier: "ExerciseSceneVC") as! ExerciseViewController
//            self.navigationController?.pushViewController(exerciseVC, animated: true)
        
        if sender.tag == 1 {
            let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
             self.navigationController?.show(storySceneVC, sender: nil)
        } else if sender.tag == 2 {
            print("next tapped")
            let exerciseVC = UIStoryboard(name: "Exercise", bundle: nil).instantiateViewController(withIdentifier: "ExerciseSceneVC") as! ExerciseViewController
           
        
           
//            if exerciseLevel == 0 {
//                let boardImageViews = [UIImageView(image: #imageLiteral(resourceName: "empty green border")),UIImageView(image: #imageLiteral(resourceName: "empty red border")),UIImageView(image: #imageLiteral(resourceName: "empty yellow border"))]
//                exerciseVC.borderImageViews = boardImageViews
//                exerciseVC.exerciseLevel = 1
//                self.navigationController?.show(exerciseVC, sender: nil)
//            } else if exerciseLevel == 1 {
//                let boardImageViews = [UIImageView(image: #imageLiteral(resourceName: "contorno grigio senza scritte ")),UIImageView(image: #imageLiteral(resourceName: "contorno grigio senza scritte ")),UIImageView(image: #imageLiteral(resourceName: "contorno grigio senza scritte "))]
//                exerciseVC.borderImageViews = boardImageViews
//                exerciseVC.exerciseLevel = 2
//                 self.navigationController?.show(exerciseVC, sender: nil)
//            } else {
//                let homeSceneVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeSceneVC") as! HomeViewController
//                self.navigationController?.show(homeSceneVC, sender: nil)
//
//            }
            

        }
    }
 
    @IBAction func onClickButton(_ sender: UIButton) {
        
        let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
        self.navigationController?.show(storySceneVC, sender: nil)
//        self.navigationController?.popViewController(animated: true)
        
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.operation = operation
        return transition
    }
    
        
    
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer){
        let translation = panGesture.translation(in: self.view)
//            let velocityInView = panGesture.velocity(in: view)
            
            switch panGesture.state {
            case .began:
                break
            case .changed:
                exampleCard.frame.origin = CGPoint(
                    x: originalPosition!.x - exampleCard.bounds.midX + translation.x,
                    y: originalPosition!.y  - exampleCard.bounds.midY + translation.y
                )
                        break
            case .ended:
    //            delegate?.panGestureDidEnd?(panGesture, originalCenter: originalPosition!, translation: translation, velocityInView: velocityInView)
                if exampleCard.frame.intersects(UIImageView(image: UIImage(named: "contorno1")).frame){
                    print("ciupa")
                }
                print("oof")
                break
            default:
                                break
            }
            
        }
    
    
}
