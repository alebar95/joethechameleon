//
//  HomeViewController.swift
//  JoeTheChameleon
//
//  Created by Alessandro Barruffo on 29/02/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit
import AVFoundation

var backgroundMusicPlayer : AVAudioPlayer = AVAudioPlayer()


class HomeViewController: UIViewController,  UIScrollViewDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var soundButton: UIButton!
    let transition = RevealAnimator()
    var numberOfChapters = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        let slides = createSlides()
        setUpScrollView(slides : slides)
        scrollView.delegate = self
        
        pageControl.numberOfPages = 2
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
        do {
            backgroundMusicPlayer = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "Joe_BGM", ofType: "mp3")!)!)
            backgroundMusicPlayer.numberOfLoops = -1
            backgroundMusicPlayer.volume = 0.5
            backgroundMusicPlayer.prepareToPlay()
        } catch {
            print(error)
        }
        audioSetting()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        audioSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    
    @IBAction func onClickColour(_ sender: UIButton) {
        let colourSceneVC = UIStoryboard(name: "Colour", bundle: nil).instantiateViewController(withIdentifier: "ColourSceneVC") as! ColourViewController
        self.navigationController?.pushViewController(colourSceneVC, animated: true)
    }
    
    func createSlides() -> [Slide] {
        let slide1: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.chapterButton.setImage(UIImage(named: "Chapter1"),for: .normal)
        slide1.chapterButton.tag = 1
        slide1.chapterButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        let slide2: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.chapterButton.setImage(UIImage(named: "Chapter2"), for: .normal)
        return [slide1,slide2 ]
    }
    
    
    func setUpScrollView(slides: [Slide] ) {
        contentView.addSubview(scrollView)
        scrollView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        
        scrollView.contentSize = CGSize(width: contentView.frame.width * CGFloat(slides.count), height: contentView.frame.height )
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: contentView.frame.width * CGFloat(i), y: 0, width: contentView.frame.width, height: contentView.frame.height)
            scrollView.addSubview(slides[i])
        }
        
        
    }
    
    @objc func buttonAction(sender: UIButton) {
        if sender.tag == 1 {
            let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
            self.navigationController?.pushViewController(storySceneVC, animated : true)
        }
    }
    
    
    @IBAction func onClickMute_Unmute(_ sender: UIButton) {
        if Settings.shared.isAudioOn {
            sender.setImage(#imageLiteral(resourceName: "icona no volume"), for: .normal)
            backgroundMusicPlayer.volume = 0
        } else {
            sender.setImage(#imageLiteral(resourceName: "icona volume attivo"), for: .normal)
            backgroundMusicPlayer.volume = 0.5
            backgroundMusicPlayer.play()
        }
        Settings.shared.isAudioOn.toggle()
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.operation = operation
        return transition
    }
    
    func audioSetting() {
        if Settings.shared.isAudioOn {
            soundButton.setImage(#imageLiteral(resourceName: "icona volume attivo"),for: .normal)
            backgroundMusicPlayer.play()
            
        } else {
            soundButton.setImage(#imageLiteral(resourceName: "icona no volume"),for: .normal)
            backgroundMusicPlayer.volume = 0
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
