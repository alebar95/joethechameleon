//
//  TryExerciseViewController.swift
//  Speekeezy
//
//  Created by Andrea Cascella on 06/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit



class ExerciseViewController: UIViewController, DraggableViewDelegate {
       
    @IBOutlet weak var ExerciseView: UIView!
     
    let imageView = UIImageView(image: #imageLiteral(resourceName: "Well done!"))
    let transition = RevealAnimator()
    var repeatButton = UIButton()
    var nextButton = UIButton()

    
    
    
    var cards : [CardView] = []
    var cells : [CellView] = []
    
    var cardsCellsDict : [CardView: CellView] = [:]
    var cardsCentersDict : [CardView : CGPoint] = [:]
    
    let phrase = [Part(partType: .subject, partName: "Joe"), Part(partType: .verb, partName: "Goes to"), Part(partType: .object, partName: "The kindergarten")]
    
    var Exercise1 = Exercise(name: "Exercise1")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        audioSetting()
        dragInteraction = UIDragInteraction(delegate: self)
        for i in 0 ..< 3 {
            if borderImageViews[i] != nil {
                
                if i == 0 {
                    greenBoardImageView.image = borderImageViews[i]?.image
                } else if i == 1 {
                    redBoardImageView.image = borderImageViews[i]?.image
                } else if i == 2 {
                    yellowBoardImageView.image = borderImageViews[i]?.image
                }
            }
        }
        repeatButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
        repeatButton.tag = 1
        
        view.addSubview(repeatButton)
        repeatButton.alpha = 0.0
        repeatButton.translatesAutoresizingMaskIntoConstraints = false
        repeatButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -85).isActive = true
        repeatButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 300).isActive = true
 
        
        nextButton.setImage(#imageLiteral(resourceName: "icona forward.png"), for: .normal)
        nextButton.tag = 2
        view.addSubview(nextButton)
        nextButton.alpha = 0.0
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 85).isActive = true
        nextButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 300).isActive = true
        
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.center.x = view.center.x
        imageView.center.y = view.center.y+1000
        
        
        
        
        
        ExerciseView.backgroundColor = .white
        
        summonThings(phrase: phrase, exercise: Exercise1)
        
        Exercise1.phrase = phrase
        
        
        for card in cards {
            Exercise1.card = card
        }
        
        for cell in cells{
            Exercise1.cell = cell
        }
        
        Exercise1.cardsCellsDict = cardsCellsDict
        
        Exercise1.populateReference()
        
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func summonThings(phrase: [Part], exercise: Exercise){
        
        let numberOfElements = phrase.count
        
        let width = ExerciseView.frame.width / CGFloat(numberOfElements)
        let height = ExerciseView.frame.height / 2.0
        
        var centerX =  width / 2.0
        let centerY = height / 2.0
        
        for i in 0...numberOfElements-1 {
            
            print(numberOfElements)
            
            let cellCenter = CGPoint(x: centerX, y: centerY*1.1)
            let cardCenter = CGPoint(x: centerX, y: (centerY*1.1) + height)
            //            let size = CGSize(width: width*0.5, height: height*0.7)
            let size = CGSize(width: width*0.7, height: height*0.8)
            
            let cell = makeCell(part: phrase[i], center: cellCenter, size: size)
            let card = makeCard(part: phrase[i], center: cardCenter, exercise: exercise)
            
            print("created \(cell) at \(cellCenter)")
            print("created \(card) at \(cardCenter)")
            print("-----")
            
            card.frame = cell.frame
            
            cards.append(card)
            cells.append(cell)
            cardsCellsDict[card] = cell
            
            ExerciseView.addSubview(cell)
            ExerciseView.sendSubviewToBack(cell)
            ExerciseView.addSubview(card)
            ExerciseView.bringSubviewToFront(card)
            
            centerX += width
        }
        
        var tempOrigins : [CGPoint] = []
        
        
        for card in cards{
            tempOrigins.append(card.resetPosition!)
        }
        tempOrigins.shuffle()
        while tempOrigins[0] == cards[0].center{
            tempOrigins.shuffle()
        }
        
        for card in 0...cards.count-1{
            cardsCentersDict.updateValue(tempOrigins[card], forKey: cards[card])
            cards[card].center = cardsCentersDict[cards[card]]!
            cards[card].resetPosition = cards[card].center
        }
        
    }
    
    func makeCard(part: Part, center: CGPoint, exercise: Exercise) -> CardView {
        
        let card = CardView()
        
        
        card.center = center
        card.resetPosition = center
        
        card.backgroundColor = .clear
        card.Card.backgroundColor = part.color
                
        card.cardImage.image = UIImage(named: "\(exercise.name)/\(part.partType)")
        card.cardImage.dropShadow(scale: true)
        card.sendSubviewToBack(card.cardImage)
        
        card.cardText(string: part.partName )
        
        card.cardType = part.partType
        
        card.delegate = self
        
        return card
        
    }
    
    func makeCell(part: Part, center: CGPoint, size: CGSize) -> CellView {
        
        let cell = CellView()
        
        cell.frame.size = size
        cell.center = center
        
        cell.cellType = part.partType
        
        cell.cellLabel.text = String(part.question)
                
        cell.cellLabel.textColor = part.color
        cell.addDashedBorder(color: part.color)
        
        return cell
        
    }
    
    func panGestureDidBegin(_ panGesture: UIPanGestureRecognizer, originalCenter: CGPoint) {
        
        
        print("began")
    }
    
    func panGestureDidEnd(_ panGesture: UIPanGestureRecognizer, originalCenter: CGPoint, translation: CGPoint, velocityInView: CGPoint) {
        print(originalCenter)
        
        Exercise1.card = (panGesture.view as? CardView)!
        
        Exercise1.exerciseLogic()
        
        if Exercise1.compareToReference() == true {
            
            
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5 , initialSpringVelocity: 1.0, options: [.curveEaseOut, .transitionFlipFromBottom], animations: {
                self.view.addSubview(self.imageView)
                self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
                self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor,constant: 0).isActive = true
                self.view.layoutIfNeeded()
                
            }) { _ in
                 print("YEA")
            }
            
            
            
        }
        
    }
 
        @IBAction func onClickButton(_ sender: UIButton) {
            
            let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
            self.navigationController?.show(storySceneVC, sender: nil)
    //        self.navigationController?.popViewController(animated: true)
            
        }
    }
 
    @IBAction func onClickButton(_ sender: UIButton) {
        
        let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
        self.navigationController?.show(storySceneVC, sender: nil)
//        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func onClickMute_UnMute(_ sender: UIButton) {
        if Settings.shared.isAudioOn {
                  audioButton.setImage(#imageLiteral(resourceName: "icona no volume"), for: .normal)
                  audioPlayer.volume = 0
              } else {
                  audioButton.setImage(#imageLiteral(resourceName: "icona volume attivo"), for: .normal)
                  audioPlayer.volume = 1
              }
        Settings.shared.isAudioOn.toggle()
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.operation = operation
        return transition
    }
    
    func audioSetting() {
        if Settings.shared.isAudioOn {
            audioButton.setImage(#imageLiteral(resourceName: "icona volume attivo"), for: .normal)
            audioPlayer.volume = 1
        } else {
            audioButton.setImage(#imageLiteral(resourceName: "icona no volume"), for: .normal)
            audioPlayer.volume = 0
        }
    }
    
    
    
}
