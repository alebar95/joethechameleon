//
//  ViewController.swift
//  JoeTheChameleon
//
//  Created by Alessandro Barruffo on 20/02/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit
import AVFoundation


class ColourViewController: UIViewController {

    @IBOutlet weak var chameleonImageView: UIImageView!

    var lastPoint = CGPoint.zero
    var color = UIColor.black
    var brushWidth: CGFloat = 20.0
    var opacity: CGFloat = 1.0
    var swiped = false
    var player = AVAudioPlayer()
    let maskImageView = UIImageView()
    let chameleonLayer = CALayer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//                var backImage = UIImage(named: "icona back")
            
//                backImage = resizeImage(image: backImage!, newWidth: 100) //the width that you want for the back button image
        //        UINavigationBar.appearance().backIndicatorImage = backImage
//               self.navigationController?.navigationBar.backIndicatorImage = backImage
//               self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
              self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
      
        chameleonImageView.layer.cornerRadius = 100
        backgroundMusicPlayer.stop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        audioSetting()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        do{
            player = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "pencil", ofType: "wav")!)!)
            player.rate = 2.0
            
            player.numberOfLoops = -1
            player.prepareToPlay()
        } catch {
            print(error)
        }
        if Settings.shared.isAudioOn {
              player.play()
        }
        swiped = false
        lastPoint = touch.location(in: chameleonImageView)
    }
    
    func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        UIGraphicsBeginImageContext(chameleonImageView.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        chameleonImageView.image?.draw(in: chameleonImageView.bounds)
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(brushWidth)
        context.setStrokeColor(color.cgColor)
        context.strokePath()
        chameleonImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        chameleonImageView.alpha = opacity
        
        maskImageView.image = UIImage(named: "chameleon_Mask")
        maskImageView.frame = chameleonImageView.bounds
        chameleonImageView.mask = maskImageView
        
        chameleonLayer.contents = UIImage(named: "chameleon_Layer")?.cgImage
        chameleonLayer.frame = chameleonImageView.bounds
        chameleonImageView.layer.addSublayer(chameleonLayer)
        
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
      guard let touch = touches.first else {
        return
      }
      swiped = true
        let currentPoint = touch.location(in: chameleonImageView)
      drawLine(from: lastPoint, to: currentPoint)
      lastPoint = currentPoint
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        player.stop()
    }

    @IBAction func onClickHome(_ sender: UIButton) {
        let storySceneVC = UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: "StorySceneVC") as! StoryViewController
        storySceneVC.chameleonImageView  = UIImageView(image: chameleonImageView.image)
        Character.shared.image = chameleonImageView.image!
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func pencliPressed(_ sender: UIButton) {
        guard let pencil = Pencil(tag: sender.tag) else {
            return
        }
        do{
            player = try AVAudioPlayer(contentsOf: URL.init(string: Bundle.main.path(forResource: "woodblock_0\(Int.random(in: 1 ... 4))", ofType: "wav")!)!)
            player.prepareToPlay()
        } catch {
            print(error)
        }
        if Settings.shared.isAudioOn {
            player.play()
        }
        self.color = pencil.color
    }
    
    func updateConstraints(view: UIView){
        view.translatesAutoresizingMaskIntoConstraints = false
        UIView.animate(withDuration: 0.3) {
            view.center.x -= 50
        }
    }
    
    
    func audioSetting() {
        if Settings.shared.isAudioOn {
            player.volume = 0.05
        } else {
            player.volume = 0
        }
    }
}

