//
//  RevealAnimator.swift
//  Speekeezy
//
//  Created by Alessandro Barruffo on 05/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit

class RevealAnimator: NSObject, UIViewControllerAnimatedTransitioning,  CAAnimationDelegate {
    
    let animationDuration = 0.5
    var operation: UINavigationController.Operation = .push
    weak var storedContext: UIViewControllerContextTransitioning?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        storedContext = transitionContext
        
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to) else {
                print("error")
                transitionContext.completeTransition(false)
                return
        }
        
        if operation == .push {
            let duration = transitionDuration(using: transitionContext)
            transitionContext.containerView.addSubview(toVC.view)
            toVC.view.frame = transitionContext.finalFrame(for: toVC)
            if fromVC is ExerciseViewController  && toVC is StoryViewController {
                
                toVC.view.clipsToBounds = true
                toVC.view.frame = CGRect(x: -fromVC.view.frame.width, y: 0, width: fromVC.view.frame.width, height: fromVC.view.frame.height)
                UIView.animate(withDuration: duration,
                               delay: 0.0, options: [],
                               animations: {
                                toVC.view.center.x += fromVC.view.frame.width
                }) { _  in
                    transitionContext.completeTransition(true)
                }
                
            }
            if fromVC is StoryViewController  && toVC is ExerciseViewController {
                
                toVC.view.clipsToBounds = true
                toVC.view.frame = CGRect(x: fromVC.view.frame.width, y: 0, width: fromVC.view.frame.width, height: fromVC.view.frame.height)
                UIView.animate(withDuration: duration,
                               delay: 0.0, options: [],
                               animations: {
                                toVC.view.center.x -= fromVC.view.frame.width
                }) { _  in
                    transitionContext.completeTransition(true)
                }
            }
            if fromVC is StoryViewController && toVC is HomeViewController {
                toVC.view.clipsToBounds = true
                toVC.view.transform = CGAffineTransform(scaleX: 0, y: 0)

                UIView.animate(withDuration: duration,
                               delay: 0.0,usingSpringWithDamping: 0.8 ,
                               initialSpringVelocity: 0.1, options: [.transitionCurlDown],
                               animations: {
                              toVC.view.transform = CGAffineTransform(scaleX: 1, y: 1)
                }) { _  in
                    transitionContext.completeTransition(true)
                }
                
            }
            
            presentPushAnimation(with: transitionContext, viewToAnimate: toVC.view)
    
        }
        else if operation == .pop {
            transitionContext.containerView.addSubview(toVC.view)
            toVC.view.frame = transitionContext.finalFrame(for: toVC)
            presentPopAnimation(with: transitionContext, viewToAnimate: toVC.view)
            
        }
    }
    
    
    
    
    func presentPushAnimation(with transitionContext: UIViewControllerContextTransitioning, viewToAnimate: UIView) {
        viewToAnimate.clipsToBounds = true
        viewToAnimate.transform = CGAffineTransform(scaleX: 0, y: 0)
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,
                       delay: 0.0, usingSpringWithDamping: 0.8 ,
                       initialSpringVelocity: 0.1, options: [],
                       animations: {
                        viewToAnimate.transform = CGAffineTransform(scaleX: 1.0, y: 1.0 )
        }) { _  in
            transitionContext.completeTransition(true)
        }
    }
    
    func presentPopAnimation(with transitionContext: UIViewControllerContextTransitioning, viewToAnimate: UIView) {
       guard let fromVC = transitionContext.viewController(forKey: .from),
        let toVC = transitionContext.viewController(forKey: .to) else {
            transitionContext.completeTransition(false)
            return
        }
        
        toVC.view.clipsToBounds = true
        toVC.view.frame = CGRect(x: fromVC.view.frame.width, y: 0, width: fromVC.view.frame.width, height: fromVC.view.frame.height)
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,
                       delay: 0.0, options: [],
                       animations: {
                        toVC.view.center.x -= fromVC.view.frame.width
        }) { _  in
            transitionContext.completeTransition(true)
        }
        
        
    }
    
    
    
    
    
}
