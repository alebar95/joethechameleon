//
//  Character.swift
//  JoeTheChameleon
//
//  Created by Alessandro Barruffo on 02/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import Foundation
import UIKit

class Character {
    
    static let shared = Character()
    var image = #imageLiteral(resourceName: "chameleon_White chameleon")
    private init() {
    }
}




