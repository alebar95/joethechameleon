//
//  CardView.swift
//  Speekeezy
//
//  Created by Andrea Cascella on 05/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit

class CardView: UIView {
    
    @IBOutlet var card: UIView!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    

    var panGestureRecognizer : UIPanGestureRecognizer?
    var originalPosition : CGPoint?
    var resetPosition : CGPoint?
    weak var delegate : DraggableViewDelegate?
    var cardType : PartType?
    
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        cardInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        cardInit()
    }
    
    private func cardInit(){
        
        resetPosition = self.center
        
        
        isUserInteractionEnabled = true
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
        self.addGestureRecognizer(panGestureRecognizer!)
        
        
        
        Bundle.main.loadNibNamed("CardView", owner: self, options: nil)
        
        
        card.frame = self.bounds
        card.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        card.layer.cornerRadius = 20
        
        //        Card.superview?.layer.masksToBounds = true
        
        
        
        
        cardImage.layer.shadowColor = UIColor.black.cgColor
        cardImage.layer.shadowRadius = 5
        
        cardLabel.layer.masksToBounds = false
        
        card.layer.shadowColor = UIColor.black.cgColor
        card.layer.shadowOpacity = 0
        card.layer.shadowOffset = CGSize(width: 0, height: 1)
        card.layer.shadowRadius = 20
        
        
        
        
        addSubview(card)
        
    }
    
    func cardText(string: String){
        
        
        let shadow = NSShadow()
        shadow.shadowBlurRadius = 5
        shadow.shadowColor = UIColor.black.withAlphaComponent(0.5)
        
        let strokeTextAttributes: [NSAttributedString.Key : Any] = [
            .strokeColor : UIColor.white,
            .foregroundColor : self.card.backgroundColor!,
            .strokeWidth : -5.0,
            .shadow : shadow
        ]
        
        cardLabel.fitTextToBounds()
        cardLabel.attributedText = NSAttributedString(string: string, attributes: strokeTextAttributes)
//        cardLabel.adjustsFontSizeToFitWidth = true
    }
    
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: superview)
        let velocityInView = panGesture.velocity(in: superview)
        
        switch panGesture.state {
        case .began:
            originalPosition = self.center
            
            self.layer.shadowOpacity = 0.5
            
            delegate?.panGestureDidBegin?(panGesture, originalCenter: originalPosition!)
            break
        case .changed:
            self.frame.origin = CGPoint(
                x: originalPosition!.x - self.bounds.midX + translation.x,
                y: originalPosition!.y  - self.bounds.midY + translation.y
            )
            delegate?.panGestureDidChange?(panGesture, originalCenter: originalPosition!, translation: translation, velocityInView: velocityInView)
            break
        case .ended:
            
            self.layer.shadowOpacity = 0
            
            delegate?.panGestureDidEnd?(panGesture, originalCenter: originalPosition!, translation: translation, velocityInView: velocityInView)
            break
        default:
            delegate?.panGestureStateToOriginal?(panGesture, originalCenter: originalPosition!, translation: translation, velocityInView: velocityInView)
            break
        }
    }
    
    
    
    
    
    

    
    
    
    
}




@objc
protocol DraggableViewDelegate: class {
    @objc optional func panGestureDidBegin(_ panGesture:UIPanGestureRecognizer, originalCenter:CGPoint)
    @objc optional func panGestureDidChange(_ panGesture:UIPanGestureRecognizer,originalCenter:CGPoint, translation:CGPoint, velocityInView:CGPoint)
    @objc optional func panGestureDidEnd(_ panGesture:UIPanGestureRecognizer, originalCenter:CGPoint, translation:CGPoint, velocityInView:CGPoint)
    @objc optional func panGestureStateToOriginal(_ panGesture:UIPanGestureRecognizer,originalCenter:CGPoint,  translation:CGPoint, velocityInView:CGPoint)
}


extension UIFont {
    
    /**
     Will return the best font conforming to the descriptor which will fit in the provided bounds.
     */
    static func bestFittingFontSize(for text: String, in bounds: CGRect, fontDescriptor: UIFontDescriptor, additionalAttributes: [NSAttributedString.Key: Any]? = nil) -> CGFloat {
        let constrainingDimension = min(bounds.width, bounds.height)
        let properBounds = CGRect(origin: .zero, size: bounds.size)
        var attributes = additionalAttributes ?? [:]
        
        let infiniteBounds = CGSize(width: CGFloat.infinity, height: CGFloat.infinity)
        var bestFontSize: CGFloat = constrainingDimension
        
        for fontSize in stride(from: bestFontSize, through: 0, by: -1) {
            let newFont = UIFont(descriptor: fontDescriptor, size: fontSize)
            attributes[.font] = newFont
            
            let currentFrame = text.boundingRect(with: infiniteBounds, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: attributes, context: nil)
            
            if properBounds.contains(currentFrame) {
                bestFontSize = fontSize
                break
            }
        }
        return bestFontSize
    }
    
    static func bestFittingFont(for text: String, in bounds: CGRect, fontDescriptor: UIFontDescriptor, additionalAttributes: [NSAttributedString.Key: Any]? = nil) -> UIFont {
        let bestSize = bestFittingFontSize(for: text, in: bounds, fontDescriptor: fontDescriptor, additionalAttributes: additionalAttributes)
        return UIFont(descriptor: fontDescriptor, size: bestSize)
    }
}

extension UILabel {
    
    /// Will auto resize the contained text to a font size which fits the frames bounds.
    /// Uses the pre-set font to dynamically determine the proper sizing
    func fitTextToBounds() {
        guard let text = text, let currentFont = font else { return }
    
        let bestFittingFont = UIFont.bestFittingFont(for: text, in: bounds, fontDescriptor: currentFont.fontDescriptor, additionalAttributes: basicStringAttributes)
        font = bestFittingFont
    }
    
    private var basicStringAttributes: [NSAttributedString.Key: Any] {
        var attribs = [NSAttributedString.Key: Any]()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = self.textAlignment
        paragraphStyle.lineBreakMode = self.lineBreakMode
        attribs[.paragraphStyle] = paragraphStyle
        
        return attribs
    }
}

extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}



