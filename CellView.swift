//
//  CellView.swift
//  Speekeezy
//
//  Created by Andrea Cascella on 06/03/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import UIKit



class CellView: UIView {

    @IBOutlet weak var Cell: UIView!
    @IBOutlet weak var cellLabel: UILabel!
    

    var cellType : PartType?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        cellInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        cellInit()
    }
    
    private func cellInit(){
        
        
        
        Bundle.main.loadNibNamed("CellView", owner: self, options: nil)
        
        
        addSubview(Cell)
        Cell.frame = self.bounds
        Cell.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        Cell.layer.masksToBounds = true
        
        Cell.addDashedBorder(color: UIColor.black)
        
        cellLabel.translatesAutoresizingMaskIntoConstraints = false
        cellLabel.centerXAnchor.constraint(equalTo: Cell.centerXAnchor).isActive = true
        cellLabel.centerYAnchor.constraint(equalTo: Cell.centerYAnchor).isActive = true 
        cellLabel.textColor = UIColor.black
        cellLabel.text = "Prova"
        
        
       
        
    }
    
    

}

extension UIView{
    func addDashedBorder(color: UIColor) {
        
        let color = color.cgColor

    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    
    shapeLayer.lineWidth = 4
        shapeLayer.lineCap = .round
    shapeLayer.lineDashPattern = [2,8]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 20).cgPath

    self.layer.addSublayer(shapeLayer)
    }
}
